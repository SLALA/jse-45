package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = "./commands.txt";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "./messages.txt";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = "./errors.txt";

    private static final String CONFIG_FILE = "/logger.properties";

    private final LogManager logManager = LogManager.getLogManager();

    private final Logger commands = Logger.getLogger(COMMANDS);

    private final Logger messages = Logger.getLogger(MESSAGES);

    private final Logger errors = Logger.getLogger(ERRORS);

    private final Logger root = Logger.getLogger("");

    {
        applyConfiguration();
        init(commands, COMMANDS_FILE, false);
        init(messages, MESSAGES_FILE, true);
        init(errors, ERRORS_FILE, true);
    }

    private void applyConfiguration() {
        try {
            logManager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return consoleHandler;
    }

    private void init(final Logger logger, final String filePath, final boolean isConsole) {
        try {
            logger.setUseParentHandlers(false);
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.addHandler(new FileHandler(filePath));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        Optional.ofNullable(message).filter((i) -> !i.isEmpty()).ifPresent(messages::info);
    }

    @Override
    public void commands(@Nullable final String message) {
        Optional.ofNullable(message).filter((i) -> !i.isEmpty()).ifPresent(commands::info);
    }

    @Override
    public void errors(@Nullable final Throwable e) {
        Optional.ofNullable(e).ifPresent((error) -> errors.log(Level.SEVERE, error.getMessage() + " [Error Class]:" + error.getClass() + " [Error Cause]:" + error.getCause(), error));
    }

}
