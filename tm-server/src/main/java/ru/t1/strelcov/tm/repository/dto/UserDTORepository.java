package ru.t1.strelcov.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.strelcov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.Optional;

public final class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public Class<UserDTO> getClazz() {
        return UserDTO.class;
    }

    @SneakyThrows
    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.login = :login";
        return entityManager.createQuery(jpql, getClazz()).setHint("org.hibernate.cacheable", true).setParameter("login", login).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@NotNull final String login) {
        @NotNull final Optional<UserDTO> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

    @SneakyThrows
    @Override
    public boolean loginExists(@NotNull final String login) {
        @NotNull final String jpql = "SELECT COUNT(e) > 0 FROM " + getEntityName() + " e WHERE e.login = :login";
        return entityManager.createQuery(jpql, Boolean.class).setHint("org.hibernate.cacheable", true).setParameter("login", login).getSingleResult();
    }

}
