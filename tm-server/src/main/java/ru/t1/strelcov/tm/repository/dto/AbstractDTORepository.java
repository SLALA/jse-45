package ru.t1.strelcov.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.dto.IDTORepository;
import ru.t1.strelcov.tm.dto.model.AbstractEntityDTO;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDTORepository<E extends AbstractEntityDTO> implements IDTORepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    public abstract Class<E> getClazz();

    @NotNull
    public String getEntityName() {
        return getClazz().getSimpleName();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final String jpql = "FROM " + getEntityName();
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

    @Override
    public void addAll(@NotNull final List<E> entities) {
        entities.forEach((e) -> {
            add(e);
            entityManager.flush();
        });
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getEntityName();
        entityManager.createQuery(jpql).executeUpdate();
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull final E entity) {
        entityManager.remove(entity);
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
