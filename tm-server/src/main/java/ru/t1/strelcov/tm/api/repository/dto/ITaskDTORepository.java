package ru.t1.strelcov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IBusinessDTORepository<TaskDTO> {

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
