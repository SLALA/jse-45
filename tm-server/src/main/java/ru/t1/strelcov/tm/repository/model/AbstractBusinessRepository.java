package ru.t1.strelcov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.model.IBusinessRepository;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.user.id = :userId";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final String sortColumnName) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.user.id = :userId ORDER BY e." + sortColumnName;
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @SneakyThrows
    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM " + getEntityName() + " e WHERE e.user.id = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.user.id = :userId AND e.name = :name";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).setParameter("name", name).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.user.id = :userId AND e.id = :id";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByName(userId, name));
        entity.map(AbstractBusinessEntity::getId).ifPresent(this::removeById);
        return entity.orElse(null);
    }

}
