package ru.t1.strelcov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.dto.ITaskDTOService;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskDTOServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDTOService service = new TaskDTOService(connectionService);

    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @NotNull
    UserDTO testUser = new UserDTO("test2", "test", Role.USER);

    @NotNull
    UserDTO testUserUnique = new UserDTO("testUnique", "test", Role.USER);

    @NotNull
    String testUserId = testUser.getId();

    @NotNull
    String testUserUniqueId = testUserUnique.getId();

    @NotNull
    private static final String NAME_UNIQUE = "NAME_UNIQUE";

    @NotNull
    final List<TaskDTO> tasks = Arrays.asList(
            new TaskDTO(testUserId, "pr1"),
            new TaskDTO(testUserId, "pr4"),
            new TaskDTO(testUserId, "pr3"));

    @Before
    public void before() {
        try {
            @NotNull UserDTO existedUser = userService.findByLogin(testUser.getLogin());
            userService.removeByLogin(existedUser.getLogin());
            service.clear(existedUser.getId());
        } catch (@NotNull Exception ignored) {
        }
        try {
            @NotNull UserDTO existedUser = userService.findByLogin(testUserUnique.getLogin());
            userService.removeByLogin(existedUser.getLogin());
            service.clear(existedUser.getId());
        } catch (@NotNull Exception ignored) {
        }
        userService.add(testUser);
        userService.add(testUserUnique);
        service.addAll(tasks);
    }

    @After
    public void after() {
        service.clear(testUserId);
        service.clear(testUserUniqueId);
        userService.removeById(testUserId);
        userService.removeById(testUserUniqueId);
        connectionService.closeFactory();
    }

    @Test
    public void addTest() {
        @NotNull final TaskDTO task = new TaskDTO(testUserId, "pr1");
        int size = service.findAll(testUserId).size();
        service.add(null);
        Assert.assertEquals(size, service.findAll(testUserId).size());
        service.add(task);
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(task));
    }

    @Test
    public void addAllTest() {
        @NotNull final TaskDTO[] newTasks = new TaskDTO[]{new TaskDTO(testUserId, "pr11"), new TaskDTO(testUserId, "pr22")};
        @NotNull final ArrayList<TaskDTO> list = new ArrayList<>(Arrays.asList(newTasks));
        int size = service.findAll(testUserId).size();
        int listSize = list.size();
        @NotNull final ArrayList<TaskDTO> listWithNulls = new ArrayList<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).containsAll(list));
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(task.getId())));
            Assert.assertEquals(task, service.findById(task.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final TaskDTO task = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(task.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void updateByIdByUserIdTest() {
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(null, id, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, id, "", "newDesc"));
            service.updateById(userId, id, "newName", "newDesc");
            Assert.assertEquals("newName", service.findById(userId, id).getName());
            Assert.assertEquals("newDesc", service.findById(userId, id).getDescription());
            Assert.assertNotNull(service.updateById(userId, id, "newName", null));
            Assert.assertNull(service.findById(userId, id).getDescription());
        }
    }

    @Test
    public void updateByNameByUserIdTest() {
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUserId();
            @NotNull final String name = task.getName();
            @Nullable final String newName = "newName";
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, null, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(null, name, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, name, null, "newDesc"));
            service.updateByName(userId, name, newName, "newDesc");
            Assert.assertEquals("newDesc", service.findByName(userId, newName).getDescription());
            Assert.assertEquals(newName, service.findById(task.getId()).getName());
        }
    }

    @Test
    public void changeStatusByIdByUserIdTest() {
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(null, id, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(userId, null, status));
                service.changeStatusById(userId, id, status);
                Assert.assertEquals(status, service.findById(userId, id).getStatus());
            }
        }
    }

    @Test
    public void changeStatusByNameByUserIdTest() {
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUserId();
            @NotNull final String name = task.getName();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(null, name, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, status));
                service.changeStatusByName(userId, name, status);
                Assert.assertEquals(status, service.findByName(userId, name).getStatus());
            }
            Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, Status.IN_PROGRESS));
        }
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final TaskDTO[] newTasks = new TaskDTO[]{new TaskDTO(testUserUniqueId, "pr2"), new TaskDTO(testUserUniqueId, "pr1")};
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null));
        @NotNull final ArrayList<TaskDTO> list = new ArrayList<>(Arrays.asList(newTasks));
        @NotNull final String userId = list.get(0).getUserId();
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        Assert.assertEquals(list.size(), service.findAll(userId).size());
        Assert.assertTrue(list.containsAll(service.findAll(userId)));
    }

    @Test
    public void findAllSortedByUserIdTest() {
        @NotNull final TaskDTO[] newTasks = new TaskDTO[]{new TaskDTO(testUserUniqueId, "pr2"), new TaskDTO(testUserUniqueId, "pr1")};
        @NotNull final ArrayList<TaskDTO> list = new ArrayList<>(Arrays.asList(newTasks));
        @NotNull Comparator<TaskDTO> comparator = SortType.NAME.getComparator();
        @NotNull String sort = SortType.NAME.name();
        @Nullable String userId = list.get(0).getUserId();
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null, sort));
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        @NotNull final List<TaskDTO> sortedList = service.findAll(userId, sort);
        Assert.assertEquals(list.size(), sortedList.size());
        Assert.assertTrue(list.containsAll(sortedList));
        Assert.assertEquals(sortedList.stream().sorted(comparator)
                .collect(Collectors.toList()), sortedList);
    }

    @Test
    public void findByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", "notExistedId"));
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUserId();
            @NotNull final String name = task.getName();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(userId, name)));
            Assert.assertEquals(task.getName(), service.findByName(userId, name).getName());
            Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", name));
            Assert.assertThrows(AbstractException.class, () -> service.findByName(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByName("notExistedId", null));
        @NotNull final TaskDTO task = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeByName(task.getUserId(), task.getName()));
        Assert.assertFalse(service.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void findByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", "notExistedId"));
        for (@NotNull final TaskDTO task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUserId();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(userId, task.getId())));
            Assert.assertEquals(task, service.findById(userId, task.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", task.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId", null));
        @NotNull final TaskDTO task = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(task.getUserId(), task.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void clearByUserIdTest() {
        final String userId = testUserId;
        Assert.assertThrows(AbstractException.class, () -> service.clear(null));
        @NotNull final List<TaskDTO> list = service.findAll(userId);
        int size = list.size();
        int fullSize = service.findAll().size();
        service.clear(userId);
        service.clear("notExistedId");
        Assert.assertEquals(0, service.findAll(userId).size());
        Assert.assertEquals(fullSize - size, service.findAll().size());
    }

    @Test
    public void createTest() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", "descr_new"));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, "descr_new"));
        int size = service.findAll(testUserId).size();
        Assert.assertNotNull(service.add(testUserId, NAME_UNIQUE, "descr_new"));
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(testUserId, NAME_UNIQUE)));
    }

}
