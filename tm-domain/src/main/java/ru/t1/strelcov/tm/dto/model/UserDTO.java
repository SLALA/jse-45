package ru.t1.strelcov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Objects;

@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_user")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class UserDTO extends AbstractEntityDTO {

    @Column(unique = true, nullable = false)
    @NotNull
    private String login;

    @Column(name = "password_hash", nullable = false)
    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Column(name = "first_name")
    @Nullable
    private String firstName;

    @Column(name = "last_name")
    @Nullable
    private String lastName;

    @Column(name = "middle_name")
    @Nullable
    private String middleName;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull
    private Role role = Role.USER;

    @Column(nullable = false)
    @NotNull
    private Boolean lock = false;

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @Nullable Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        if (role == null) role = Role.USER;
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + (login == null || login.isEmpty() ? "" : " : " + login);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserDTO user = (UserDTO) o;
        return login.equals(user.login) && passwordHash.equals(user.passwordHash) && Objects.equals(email, user.email) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName) && role == user.role && lock.equals(user.lock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), login, passwordHash, email, firstName, lastName, middleName, role, lock);
    }

}
