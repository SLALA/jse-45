package ru.t1.strelcov.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntityDTO extends AbstractEntityDTO implements IWBS {

    @Column(nullable = false)
    @NotNull
    private String name;

    @Nullable
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull
    private Status status = NOT_STARTED;

    @Column(nullable = false, columnDefinition = "TIMESTAMPTZ")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @NotNull
    private Date created = new Date();

    @Column(name = "user_id", nullable = false)
    @NotNull
    private String userId;

    @Column(name = "start_date", columnDefinition = "TIMESTAMPTZ")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Nullable
    private Date dateStart;

    public AbstractBusinessEntityDTO(@NotNull String userId, @NotNull String name) {
        this.userId = userId;
        this.name = name;
    }

    public AbstractBusinessEntityDTO(@NotNull String userId, @NotNull String name, @Nullable String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + " : " + name + " : " + created + " : " + userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractBusinessEntityDTO that = (AbstractBusinessEntityDTO) o;
        return name.equals(that.name) && Objects.equals(description, that.description) && status == that.status && Objects.equals(created.getTime(), that.created.getTime()) && userId.equals(that.userId) && Objects.equals(Optional.ofNullable(dateStart).map(Date::getTime).orElse(null), Optional.ofNullable(that.dateStart).map(Date::getTime).orElse(null));
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, description, status, created.getTime(), userId, Optional.ofNullable(dateStart).map(Date::getTime).orElse(null));
    }

}
