package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class TaskListSortedRequest extends AbstractUserRequest {

    @Nullable
    private String sort;

    public TaskListSortedRequest(@Nullable final String token, @Nullable final String sort) {
        super(token);
        this.sort = sort;
    }

}
